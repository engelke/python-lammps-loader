# Python LAMMPS Loader

Read LAMMPS dumps into a python data structure.

## LAMMPS Dump
Create an input script with a custom dump command

**simulation.in**
```
...
dump 1 all custom 100 simulation.dump id x y z radius mass type
```
## Installation and importing the `LammpsLoader`
Clone the repo or [download a ZIP file](https://gitlab.com/engelke/python-lammps-loader/-/archive/master/python-lammps-loader-master.zip)
```python
import os
import sys

sys.path.append('/path/to/python-lammps-loader')
from lammps_loader import LammpsLoader
```

## Loading the dump file
```python
from lammps_loader import LammpsLoader

data = LammpsLoader('path/to/simulation.dump')
```
All simulation data is stored in the array `data.timesteps`, which contains a dictionary for every time step. For example, `data.timesteps[0]['atoms']` contains the atom informations (position, type, etc.) of the first time step.

**Time Step Dictionary keys**
| Key        | Value Data Type        | LAMMPS Item           | Explanation         |
|------------|------------------------|-----------------------| --------------------|
| `timestep` | `int`                  | TIMESTEP              | Number of this timestep |
| `num_atoms`| `int`                  | NUMBER OF ATOMS       | Number of atoms in this timestep |
| `box`      | `list`                 | BOX BOUNDS            | Simulation box information<br>Contains dictionaries for each spatial dimension<br>`{'type': str, 'min': float, 'max': float}` |
| `atoms`    | Structured NumPy array | ATOMS                 | Atom information, exact content depends on the dump command |

If there are additional items present in the dump file, you can access them by using the item name as the key.
