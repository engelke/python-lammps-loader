import re
from collections import defaultdict
import numpy as np

class LammpsLoader:
    """
    Read LAMMPS dump files that were created with a dump command like this

    dump 1 all custom 100 my_simulation.dump id type radius mass x y z
    """
    # Patterns
    re_any = '[^\s]+'
    re_int = r'[+-]?\d+'
    re_number = r'[+-]?\d+.?\d*[eE]?[+-]?\d*'

    def __init__(self, filename=None, **kwargs):
        self.filename = None
        self.timesteps = []

        self.item_pattern = self.make_item_pattern()
        self.timestep_pattern = self.make_timestep_pattern()
        self.num_atoms_pattern = self.make_num_atoms_pattern()
        self.box_bounds_pattern = self.make_box_bounds_pattern()
        self.col_patterns = self.make_col_patterns()

        if filename:
            self.load(filename, **kwargs)

    def load(self, filename, **kwargs):
        self.filename = filename
        with open(filename, 'r') as f:
            pattern = self.item_pattern  # current pattern to parse line
            handler = self.parse_item    # handle match

            # go through file line by line
            for (num, line) in enumerate(f):
                m = pattern.match(line)
                try:
                    # handle match
                    if m:
                        pattern, handler = handler(pattern, m)
                    # try default pattern if match did not work
                    else:
                        pattern = self.item_pattern
                        handler = self.parse_item
                        m = pattern.match(line)
                        pattern, handler = handler(pattern, m)

                except Exception as error:
                    ignore_unknown = kwargs.get('ignore_unknown', False)
                    if not ignore_unknown:
                        print(f'{filename}:{num}: Error parsing line with pattern "{pattern.pattern}"\n> {line}> {error}')
                        return

    def make_item_pattern(self):
        return re.compile(r'ITEM:\s+(?P<item>.*)')

    def make_timestep_pattern(self):
        return re.compile(r'(?P<timestep>' + self.re_number + ')')

    def make_num_atoms_pattern(self):
        return re.compile(r'(?P<num_atoms>' + self.re_int + ')')

    def make_box_bounds_pattern(self):
        return re.compile(r'(?P<min>' + self.re_number + ')\s+(?P<max>' + self.re_number + ')')

    def make_col_patterns(self):
        col_patterns = defaultdict(lambda: self.re_number)
        col_patterns['id'] = self.re_int
        col_patterns['type'] = self.re_int
        return col_patterns

    def make_atom_pattern(self, atom_style):
        f = [r'(?P<' + s + '>' + self.col_patterns[s] + ')' for s in atom_style]
        return re.compile(r'\s'.join(f))

    def make_col_dtype(self, atom_style):
        special_dtype = {'id': int, 'type': int}
        return [(s, special_dtype[s] if s in special_dtype else float) for s in atom_style]

    def parse_item(self, pattern, m):
        item = m.group('item')
        if item == 'TIMESTEP':
            return self.timestep_pattern, self.parse_timestep

        if item == 'NUMBER OF ATOMS':
            return self.num_atoms_pattern, self.parse_num_atoms

        if item.startswith('BOX BOUNDS'):
            self.cnt = 0

            box_types = item.split()[2:]
            self.timesteps[-1]['box'] = [{'type': t} for t in box_types]
            return self.box_bounds_pattern, self.parse_box_bounds

        if item.startswith('ATOMS'):
            atom_style = item.split()[1:]
            # create empty structured array
            dtype = self.make_col_dtype(atom_style)
            self.cnt = 0
            self.timesteps[-1]['atoms'] = np.empty(self.timesteps[-1]['num_atoms'], dtype)

            return self.make_atom_pattern(atom_style), self.parse_atoms

        # Parse unknown items
        # grab all capitalized words
        m = re.match(r'(?P<name>[A-Z\s]+)\s?(?P<parameters>.*)', item)
        name = m.group('name').strip()
        parameters = m.group('parameters')
        self.timesteps[-1][name] = {'parameters': parameters.split()}
        self.timesteps[-1][name]['values'] = []
        self.target = self.timesteps[-1][name]['values']

        return re.compile('(?!ITEM:)(?P<values>.*)'), self.parse_unknown

    def parse_unknown(self, pattern, m):
        self.target.append(m.group('values').split())
        return pattern, self.parse_unknown

    def parse_timestep(self, pattern, m):
        self.timesteps.append({'timestep': int(m.group('timestep'))})
        return self.item_pattern, self.parse_item

    def parse_num_atoms(self, pattern, m):
        self.timesteps[-1]['num_atoms'] = int(m.group('num_atoms'))
        return self.item_pattern, self.parse_item

    def parse_box_bounds(self, pattern, m):
        for key, value in m.groupdict().items():
            self.timesteps[-1]['box'][self.cnt][key] = float(value)

        self.cnt += 1
        return pattern, self.parse_box_bounds

    def parse_atoms(self, pattern, m):
        self.timesteps[-1]['atoms'][self.cnt] = m.groups()
        self.cnt += 1
        return pattern, self.parse_atoms
